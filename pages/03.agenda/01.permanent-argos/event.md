---
title: 'Permanent: Argos'
type: true
simple-events:
    start: '2020-05-23'
    end: '2020-05-25'
    location: '23 rue du commerce, Brussels'
time: '18:00—22:00'
taxonomy:
    category:
        - talk
    tag:
        - Argos
        - Permanent
        - event
text: 'Who owns the city? How can we reclaim it? Following an introduction of Permanent, this session launches a collective exploration of anti-speculative urban developments for Brussels. Departing from four positions – Culture, Commons, Vacancy, Squat – invited field experts will guide a break-out session to collectively imagine urban action anew.'
links:
    argos: 'https://www.argosarts.org/'
unpublish_date: '2020-05-23 00:00'
---

