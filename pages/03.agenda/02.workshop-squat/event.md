---
title: 'Workshop squat'
type: false
simple-events:
    start: '2022-04-20'
    end: '2022-04-22'
    location: '3, rue Paul Devaux'
time: '19:00—21:00'
organiser: logement123
text: 'Workshop on squatting, how to self-organise, and sustain a living community.'
unpublish_date: '2022-04-20 00:00'
---

