---
title: Agenda
content:
    items: '@self.children'
    order:
        by: header.start
        dir: asc
    filter:
        published: true
        type: event
---

