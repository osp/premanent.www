// add your script here

function nav() {
	let btn = document.querySelector('.menu')
	  , header = document.querySelector('.header .wrapper')
	  , body = document.querySelector('section#body');

	btn.onclick = () => {
		if (btn.innerHTML != "close") {
			btn.innerHTML = "close";
		} else {
			btn.innerHTML = "menu";
		};
		if (header.style.transform != "translateX(0%)") {
			header.style.transform = "translateX(0%)";
		} else {
			header.style.transform = "translateX(-92%)";
		};
	}

	body.onclick = () => {
		if (btn.innerHTML != "menu") {
			btn.innerHTML = "menu";
		}
		if (header.style.transform != "translateX(-92%)") {
			header.style.transform = "translateX(-92%)";
		}
	};

} nav()